from django.db import models

# Create your models here.
class ScrumManager(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Developer(models.Model):
    name = models.CharField(max_length=20)
    scrum_manager = models.ForeignKey(ScrumManager, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=20)
    developer = models.ManyToManyField(Developer)

    def __str__(self):
        return self.name

