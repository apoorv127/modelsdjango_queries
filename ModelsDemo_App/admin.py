from django.contrib import admin
from ModelsDemo_App.models import ScrumManager, Developer, Project

# Register your models here.
admin.site.register(ScrumManager)
admin.site.register(Developer)
admin.site.register(Project)
