from django.apps import AppConfig


class ModelsdemoAppConfig(AppConfig):
    name = 'ModelsDemo_App'
